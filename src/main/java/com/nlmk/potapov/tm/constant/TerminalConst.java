package com.nlmk.potapov.tm.constant;

public final class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";

    public static final String BLOCK_SEPARATOR = "-----------------------------------------------";
    public static final String INDENT = "   ";
    public static final String INPUT_MESSAGE = "Task Manager> ";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";

}
